<?php namespace App\Manager;

use DateTime;

class DateCalculationManager
{
  public const MONDAY    = 1;
  public const TUESDAY   = 2;
  public const WEDNESDAY = 3;
  public const THURSDAY  = 4;
  public const FRIDAY    = 5;
  public const SATURDAY  = 6;
  public const SUNDAY    = 7;
  
  /**
   * Returns a {@see DateTime} object for the next given requested day based of a given {@see DateTime}.
   *
   * @param int           $weekDayToLookoutFor The number representing a weekday.
   *                                           Use one of the const of this class like {@see DateCalculationManager::MONDAY} to represents a day number.
   * @param DateTime|null $date                The {@see DateTime} from where the search for the weekday needs to start from.
   *                                           If nothing or null is passed the current date and time will be used.
   *
   * @return DateTime The {@see DateTime} object with the resulting date of the search for the weekday.
   */
  public function getDateOfNextGivenDayOfMonth(int $weekDayToLookoutFor, ?DateTime $date = null): DateTime
  {
    if (is_null($date)) {
      $date = new DateTime();
    }

    return $this->getDateOfBidirectionalGivenDayOfMonth($weekDayToLookoutFor, $date);
  }
  
  /**
   * Returns a {@see DateTime} object for the previous given requested day based of a given {@see DateTime}.
   *
   * @param int           $weekDayToLookoutFor The number representing a weekday.
   *                                           Use one of the const of this class like {@see DateCalculationManager::MONDAY} to represents a day number.
   * @param DateTime|null $date                The {@see DateTime} from where the search for the weekday needs to start from.
   *                                           If nothing or null is passed the current date and time will be used.
   *
   * @return DateTime The {@see DateTime} object with the resulting date of the search for the weekday.
   */
  public function getDateOfPreviousGivenDayOfMonth(int $weekDayToLookoutFor, ?DateTime $date = null): DateTime
  {
    if (is_null($date)) {
      $date = new DateTime();
    }

    return $this->getDateOfBidirectionalGivenDayOfMonth($weekDayToLookoutFor, $date, false);
  }
  
  /**
   * Same as {@see self::getDateOfNextGivenDayOfMonth()} and {@see self::getDateOfPreviousGivenDayOfMonth()} only this method holds the logic of those 2 methods.
   * 
   * @param int           $weekDayToLookoutFor The number representing a weekday.
   *                                           Use one of the const of this class like {@see DateCalculationManager::MONDAY} to represents a day number.
   * @param DateTime|null $date                The {@see DateTime} from where the search for the weekday needs to start from.
   *                                           If nothing or null is passed the current date and time will be used.
   * @param bool          $forward             If you want to search forwards or backwards.
   *
   * @return DateTime The {@see DateTime} object with the resulting date of the search for the weekday.
   */
  private function getDateOfBidirectionalGivenDayOfMonth(int $weekDayToLookoutFor, DateTime $date = null, bool $forward = true): DateTime
  {
    while (intval($date->format('N')) !== $weekDayToLookoutFor) {
      if ($forward) {
        $date->modify('+1 day');
      } else {
        $date->modify('-1 day');
      }
    }

    return $date;
  }
  
  /**
   * Gets the last day of the month in number format.
   *
   * @param int|null $month The month you want the last day from.
   *                        If null is given it will fallback to the current month.
   * @param int|null $year  The year for the month you want the last day from.
   *                        If null is given it will fallback to the current year.
   *
   * @return int The last day of the month in number format. Between 1 and 31.
   */
  public function getLastDayOfMonth(?int $month = null, ?int $year = null): int
  {
    $month = $this->resolveMonthWithFallback($month);
    $year  = $this->resolveYearWithFallback($year);

    return intval(date('t', strtotime("$year-$month-01")));
  }

  /**
   * Checks if date is a weekday.
   *
   * @param DateTime $date The date you want to check if it's a weekday.
   *
   * @return bool True for yes, false for no.
   */
  public function isWeekday(DateTime $date): bool
  {
    $dayOfWeek = date('N', $date->getTimestamp());
    
    return ($dayOfWeek >= 1 && $dayOfWeek <= 5);
  }
  public function getPrettyDateFormat(DateTime $date): string
  {
    $day = $date->format('jS');
    $monthYear = $date->format('F Y');

    return "$day of $monthYear";
  }

  /**
   * This will either return the given month as long as it's not null. If it is, it will fallback to the current month.
   *
   * @param int|null $month The month in number format. Between 1 and 12.
   *
   * @return int The month (or fallback month) in number format. Between 1 and 12.
   */
  private function resolveMonthWithFallback(?int $month = null): int
  {
    return (is_null($month)) ? intval((new DateTime())->format('m')) : $month;
  }

  /**
   * This will either return the given year as long as it's not null. If it is, it will fallback to the current year.
   *
   * @param int|null $year The year in number format like 2023.
   *
   * @return int The year (or fallback year) in number format like 2023.
   */
  private function resolveYearWithFallback(?int $year = null): int
  {
    return (is_null($year)) ? intval((new DateTime())->format('Y')) : $year;
  }
}