<?php namespace App\Manager;

use DateTime;

class PayoutSalaryDatesManager
{
  public const BONUS_PAYOUT_DAY = 15;
  
  private DateCalculationManager $dateCalculationManager;

  public function __construct(DateCalculationManager $dateCalculationManager)
  {
    $this->dateCalculationManager = $dateCalculationManager;
  }
  
  /**
   * Processes the salary and bonus payout dates for the given month and year.
   *
   * @param int $month The month in number format. Between 1 and 12.
   * @param int $year  The year in number format like 2023.
   * 
   * @return array{
   *   DateTime,
   *   DateTime
   * } Index 1 hold the salary payout date and index 2 holds the bonus payout date
   */
  public function processMonth(int $month, int $year): array
  {
    $salaryDate = $this->calculateSalaryPayoutDate($month, $year);
    $bonusDate  = $this->calculateBonusPayoutDate($month, $year);
    
    return [$salaryDate, $bonusDate];
  }
  
  /**
   * Formats the given date to the expected layout.
   *
   * @param DateTime $date The date you want formated.
   * 
   * @return string The formated date.
   */
  public function getPrettyDateFormat(DateTime $date): string
  {
    $day = $date->format('jS');
    $monthYear = $date->format('F Y');
    
    return "$day of $monthYear";
  }
  
  /**
   * Calculated the salary payout date with the given parameters.
   *
   * @param int $month The month you want the salary payout date calculated for.
   * @param int $year  The year you want the salary payout date calculated for.
   *
   * @return DateTime The calculated salary payout date for that month and year.  
   */
  private function calculateSalaryPayoutDate(int $month, int $year): DateTime
  {
    $lastDayOfMonth     = $this->dateCalculationManager->getLastDayOfMonth($month, $year);
    $lastDayOfMonthDate = new DateTime("$year-$month-$lastDayOfMonth");
    
    if ($this->dateCalculationManager->isWeekday($lastDayOfMonthDate)) {
      return $lastDayOfMonthDate;
    }
    
    return $this->dateCalculationManager->getDateOfPreviousGivenDayOfMonth(DateCalculationManager::FRIDAY, new DateTime("last day of $year-$month"));
  }

  /**
   * Calculated the bonus payout date with the given parameters.
   *
   * @param int $month The month you want the salary payout date calculated for.
   * @param int $year  The year you want the salary payout date calculated for.
   *
   * @return DateTime The calculated salary payout date for that month and year.  
   */
  private function calculateBonusPayoutDate(int $month, int $year): DateTime
  {
    $bonusPayoutDay = PayoutSalaryDatesManager::BONUS_PAYOUT_DAY;
    
    $month++;
    if ($month > 12) {
      $month = 1;
      $year++;
    }
    
    $bonusPayoutDate = new DateTime("$year-$month-$bonusPayoutDay");
    
    if ($this->dateCalculationManager->isWeekday(new DateTime("$year-$month-$bonusPayoutDay"))) {
      return $bonusPayoutDate;
    }
    
    return $this->dateCalculationManager->getDateOfNextGivenDayOfMonth(DateCalculationManager::WEDNESDAY, $bonusPayoutDate);
  }
}