<?php namespace App\Command;

use App\Manager\PayoutSalaryDatesManager;
use ErrorException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use DateTime;

#[AsCommand(
  name: 'app:payout-salary-dates',
  description: 'Determines the dates to pay salaries and bonuses.',
)]
class PayoutSalaryDatesCommand extends Command
{
  public const OPTION_OUTPUT_FILE_NAME       = 'output-name';
  public const SHORT_OPTION_OUTPUT_FILE_NAME = 'o';
  public const OPTION_YEAR                   = 'year';
  public const SHORT_OPTION_YEAR             = 'y';
    
  private PayoutSalaryDatesManager $payoutSalaryDatesManager;
  
  private string $fileName;
  
  public function __construct(PayoutSalaryDatesManager $payoutSalaryDatesManager)
  {
    parent::__construct();
    
    $this->payoutSalaryDatesManager = $payoutSalaryDatesManager;
  }

  protected function configure(): void
  {
    $this
      ->addOption(self::OPTION_OUTPUT_FILE_NAME, self::SHORT_OPTION_OUTPUT_FILE_NAME, InputOption::VALUE_OPTIONAL, 'The name you want the CSV file to have.')
      ->addOption(self::OPTION_YEAR, self::SHORT_OPTION_YEAR, InputOption::VALUE_OPTIONAL, 'Year you want to calculate payout dates.', (new DateTime())->format('Y'))
    ;
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $io         = new SymfonyStyle($input, $output);
    $year       = intval($input->getOption(self::OPTION_YEAR));
    if ($year < 1000 || $year > 9999) {
      $io->error('Year input has to be between 1000 and 9999.');
      
      return Command::INVALID;
    }

    /** @var string|null $outputName */
    $outputName = $input->getOption(self::OPTION_OUTPUT_FILE_NAME);

    if (is_null($outputName)) {
      $this->fileName = "Payout_Dates_$year.csv";
    } else {
      $this->fileName = "$outputName.csv";
    }

    $io->info("Calculating dates for year $year ...");
    try {
      $this->cleanFile();
    } catch (ErrorException $exception) {
      if (str_contains($exception->getMessage(), "Resource temporarily unavailable")) {
        $io->error('The output path could not be used. Please make sure you don\'t have the file opened.');
      } else {
        $io->error('An unexpected error has occured. ' . $exception->getMessage());
      }
      
      return Command::FAILURE;
    }

    $this->calculatePayoutDates($year);
    
    $filePath = getcwd() . '/' . $this->fileName;
    $io->success("Dates calculated. You can find the results in the file named '$filePath'.");
    
    return Command::SUCCESS;
  }
  
  /**
   * Prevents adding new data to a already existing file by deleting the old file.
   */
  private function cleanFile(): void
  {
    if (file_exists($this->fileName)) {
      if (! unlink($this->fileName)) {
        file_put_contents($this->fileName, "");
      }
    }
  }

  /**
   * Writes the given entries to the CSV file.
   *
   * @param string[] $entries The row of entries for each column.
   */
  private function writeEntryToCsvFile(array $entries): void
  {
    file_put_contents($this->fileName, implode(',', $entries) . "\n", FILE_APPEND);
  }
  
  /**
   * Calculates the payout dates for the whole given year.
   *
   * @param int $year The year you want payout dates to be calculated for.
   */
  private function calculatePayoutDates(int $year): void
  {
    $this->writeEntryToCsvFile(['Months', 'Salary payout date', 'Bonus payout date']);
    
    for ($month = 1; $month <= 12; $month++) {
      [$salaryDate, $bonusDate] = $this->payoutSalaryDatesManager->processMonth($month, $year);
      
      $monthName = date("F", mktime(0, 0, 0, $month, 1));
     
      $this->writeEntryToCsvFile([
        $monthName,
        $this->payoutSalaryDatesManager->getPrettyDateFormat($salaryDate),
        $this->payoutSalaryDatesManager->getPrettyDateFormat($bonusDate),
      ]);
    }
  }
}
