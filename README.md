# Payout Calculator

A Symfony CLI to calculate when to payout salaries and bonuses.

## Requirements

- PHP (made with 8.0.12)
- Composer (used 2.5.8)

## Installation

1. Clone this repository.
```bash
git clone https://gitlab.com/android4682/payout-calculator
```
2. Install the dependencies.
```bash
composer install --no-dev
```
3. Run the command to generate payout salary dates.
```bash
php bin/console app:payout-salary-dates
```

If you want to know more about this command pass `--help` on it.
```bash
php bin/console app:payout-salary-dates --help
```
