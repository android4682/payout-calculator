<?php namespace App\Tests\Unit\Manager;

use App\Manager\DateCalculationManager;
use DateTime;
use PHPUnit\Framework\TestCase;

class DateCalculationManagerTest extends TestCase
{
  /**
   * @covers DateCalculationManager::getDateOfNextGivenDayOfMonth
   * @covers DateCalculationManager::getDateOfBidirectionalGivenDayOfMonth
   */
  public function testGetDateOfNextGivenDayOfMonth(): void
  {
    $manager  = $this->createDateCalculationManager();
    $nextDate = $manager->getDateOfNextGivenDayOfMonth(DateCalculationManager::SUNDAY, new DateTime("2023-01-20"));
  
    self::assertEquals(22, intval($nextDate->format('d')));
    self::assertEquals(1, intval($nextDate->format('m')));
    self::assertEquals(2023, intval($nextDate->format('Y')));
  }
  
  /**
   * @covers DateCalculationManager::getDateOfPreviousGivenDayOfMonth
   * @covers DateCalculationManager::getDateOfBidirectionalGivenDayOfMonth
   */
  public function testGetDateOfPreviousGivenDayOfMonth(): void
  {
    $manager  = $this->createDateCalculationManager();
    $nextDate = $manager->getDateOfPreviousGivenDayOfMonth(DateCalculationManager::SUNDAY, new DateTime("2023-01-20"));

    self::assertEquals(15, intval($nextDate->format('d')));
    self::assertEquals(1, intval($nextDate->format('m')));
    self::assertEquals(2023, intval($nextDate->format('Y')));
  }
  
  /**
   * @covers DateCalculationManager::getDateOfPreviousGivenDayOfMonth
   */
  public function testGetLastDayOfMonth(): void
  {
    $manager = $this->createDateCalculationManager();
    
    self::assertEquals(31, $manager->getLastDayOfMonth(01, 2023));
    self::assertEquals(28, $manager->getLastDayOfMonth(02, 2023));
    self::assertEquals(30, $manager->getLastDayOfMonth(06, 2023));
  }
  
  /**
   * @covers DateCalculationManager::isWeekday
   */
  public function testIsWeekday(): void
  {
    $manager = $this->createDateCalculationManager();
    
    self::assertFalse($manager->isWeekday(new DateTime("2023-07-02")));
    self::assertTrue($manager->isWeekday(new DateTime("2023-07-04")));
  }
  
  private function createDateCalculationManager(): DateCalculationManager
  {
    return new DateCalculationManager();
  }
}