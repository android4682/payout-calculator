<?php namespace App\Tests\Unit\Manager;

use App\Manager\DateCalculationManager;
use App\Manager\PayoutSalaryDatesManager;
use DateTime;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;

class PayoutSalaryDatesManagerTest extends TestCase
{
  /**
   * @covers PayoutSalaryDatesManager::calculateSalaryPayoutDate
   *
   * @throws ReflectionException
   */
  public function testCalculateSalaryPayoutDate(): void
  {
    $manager    = $this->createPayoutSalaryDatesManager();
    $reflection = new ReflectionClass($manager);
    $method     = $reflection->getMethod('calculateSalaryPayoutDate');
    $method->setAccessible(true);
    
    /** @var DateTime $result */
    $result = $method->invoke($manager, 01, 2022);
    self::assertEquals('2022-01-31', $result->format('Y-m-d'));
  }
  
  /**
   * @covers PayoutSalaryDatesManager::calculateBonusPayoutDate
   *
   * @throws ReflectionException
   */
  public function testCalculateBonusPayoutDate(): void
  {
    $manager    = $this->createPayoutSalaryDatesManager();
    $reflection = new ReflectionClass($manager);
    $method     = $reflection->getMethod('calculateBonusPayoutDate');
    $method->setAccessible(true);
    
    /** @var DateTime $result */
    $result = $method->invoke($manager, 01, 2022);
    self::assertEquals('2022-02-15', $result->format('Y-m-d'));
  }
  
  /**
   * @covers PayoutSalaryDatesManager::getPrettyDateFormat
   */
  public function testGetPrettyDateFormat(): void
  {
    $manager = $this->createPayoutSalaryDatesManager();
    
    self::assertEquals('31st of January 2022', $manager->getPrettyDateFormat(new DateTime('2022-01-31')));
    self::assertEquals('15th of February 2022', $manager->getPrettyDateFormat(new DateTime('2022-02-15')));
  }
  
  /**
   * @covers PayoutSalaryDatesManager::processMonth
   */
  public function testProcessMonth(): void
  {
    $manager = $this->createPayoutSalaryDatesManager();
    $result  = $manager->processMonth(01, 2022);
    
    self::assertEquals('2022-01-31', $result[0]->format('Y-m-d'));
    self::assertEquals('2022-02-15', $result[1]->format('Y-m-d'));
  }
  
  private function createPayoutSalaryDatesManager(): PayoutSalaryDatesManager
  {
    return new PayoutSalaryDatesManager(new DateCalculationManager());
  }
}