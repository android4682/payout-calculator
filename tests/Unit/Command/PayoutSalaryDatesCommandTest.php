<?php namespace App\Tests\Unit\Command;

use App\Command\PayoutSalaryDatesCommand;
use App\Manager\DateCalculationManager;
use App\Manager\PayoutSalaryDatesManager;
use Exception;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class PayoutSalaryDatesCommandTest extends TestCase
{
  /**
   * @covers PayoutSalaryDatesCommand::cleanFile
   *
   * @doesNotPerformAssertions
   * 
   * @throws Exception
   */
  public function testCleanFile(): void
  {
    $payoutSalaryDatesManagerMock = $this->createMock(PayoutSalaryDatesManager::class);
    $command                      = new PayoutSalaryDatesCommand($payoutSalaryDatesManagerMock);
    $reflection                   = new ReflectionClass($command);
    $fileName                     = bin2hex(random_bytes(16)) . '.test';
      
    $property = $reflection->getProperty('fileName');
    $property->setAccessible(true);
    $property->setValue($command, $fileName);
    
    $method = $reflection->getMethod('cleanFile');
    $method->setAccessible(true);

    try {
      $method->invoke($command);
    } catch (Exception $exception) {
      $this->fail('PayoutSalaryDatesCommand::cleanFile threw an exception. ' . $exception->getMessage());
    }
    
    if (file_exists($fileName)) {
      unlink($fileName);
    }
  }
  
  /**
   * @covers PayoutSalaryDatesCommand::writeEntryToCsvFile
   *
   * @throws Exception
   */
  public function testWriteEntryToCsvFile(): void
  {
    $payoutSalaryDatesManagerMock = $this->createMock(PayoutSalaryDatesManager::class);
    $command                      = new PayoutSalaryDatesCommand($payoutSalaryDatesManagerMock);
    $reflection                   = new ReflectionClass($command);
    $fileName                     = bin2hex(random_bytes(16)) . '.test';

    $property = $reflection->getProperty('fileName');
    $property->setAccessible(true);
    $property->setValue($command, $fileName);

    $method = $reflection->getMethod('writeEntryToCsvFile');
    $method->setAccessible(true);
    $method->invoke($command, ['Test', 'Foo', 'Bar']);
    
    $content = file_get_contents($fileName);

    if (file_exists($fileName)) {
      unlink($fileName);
    }
    
    self::assertEquals("Test,Foo,Bar\n", $content);
  }
  
  /**
   * @covers PayoutSalaryDatesCommand::calculatePayoutDates
   *
   * @throws Exception
   */
  public function testCalculatePayoutDates(): void
  {
    $dateCalculationManager   = new DateCalculationManager();
    $payoutSalaryDatesManager = new PayoutSalaryDatesManager($dateCalculationManager);
    $command                  = new PayoutSalaryDatesCommand($payoutSalaryDatesManager);
    $reflection               = new ReflectionClass($command);
    $fileName                 = bin2hex(random_bytes(16)) . '.test';

    $property = $reflection->getProperty('fileName');
    $property->setAccessible(true);
    $property->setValue($command, $fileName);

    $method = $reflection->getMethod('calculatePayoutDates');
    $method->setAccessible(true);
    $method->invoke($command, 2022);
    
    $content = file_get_contents($fileName);

    if (file_exists($fileName)) {
      unlink($fileName);
    }
    
    self::assertEquals("Months,Salary payout date,Bonus payout date
January,31st of January 2022,15th of February 2022
February,28th of February 2022,15th of March 2022
March,31st of March 2022,15th of April 2022
April,29th of April 2022,18th of May 2022
May,31st of May 2022,15th of June 2022
June,30th of June 2022,15th of July 2022
July,29th of July 2022,15th of August 2022
August,31st of August 2022,15th of September 2022
September,30th of September 2022,19th of October 2022
October,31st of October 2022,15th of November 2022
November,30th of November 2022,15th of December 2022
December,30th of December 2022,18th of January 2023
", $content);
  }
}